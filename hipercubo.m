%RETORNA CUBO DE HISTOGRAMAS DE VENTANAS, RECIBE COMO PARAMENTROS LA IMAGEN
%Y LA RAIZ CUADRADA DE NO. TOTAL DE VENTANAS

function cubo = hipercubo(a, K)
mh = zeros(3,K,K,256);

[M,N,Q] = size(a);
pM = floor(M/K);
pN = floor(N/K);

%SE CREA EL CUBO DE HISTOGRAMA DE VENTANAS
for color=1:3  
    for fila=1:3
        for columna=1:3
            mh(color, fila, columna,:) = imhist(  a( 1+floor((fila-1)*pM):floor(fila*pM),      1+floor((columna-1)*pN):floor(columna*pN),      color )  );
        end
    end
end

cubo = mh;

end