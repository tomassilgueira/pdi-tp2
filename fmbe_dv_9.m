function retorno = fmbe_dv_9(img)

    img = padarray(img, [1, 1], 'replicate');     % ACA SE PADDEA LA IMAGEN RECIBIDA, Y SE RELLENAN CON EL METODO DE REPLICA LOS ESPACIOS AGREGADOS
    [M,N,Q] = size(img);    %  Q No sirve para nada, hace que las cosas funcionen nada mas
    hchist = hipercubo(img, 3); %cuboDeHist tiene dimensiones (color, fila, columna, 1:256)
    pM = floor(M/3);
    pN = floor(N/3);
    

    
    K = 3;  %Tamanho de la 
    L = 3;  %mascara de filtrado

    
    for fila=2:(M-1)
        for columna=2:(N-1)
           
            p_1 = [ceil((fila-1)/84) ceil((columna-1)/84)];
            p_4 = [ceil((fila+1)/84) ceil((columna+1)/84)];
            
            f = fila - floor(K/2);                   % CALCULA LOS
            fu = fila + floor(K/2);                  % LIMITES DE LOS
            c = columna - floor(L/2);                % SEGMENTOS
            cu = columna + floor(L/2);               
            
            red = img(f:fu,c:cu,1);                 % EXTRAE LOS
            green = img(f:fu,c:cu,2);               % SEGMENTOS R, G y B
            blue = img(f:fu,c:cu,3);                % QUE AGARRA LA MASCARA
            
            
            if p_1 == p_4
                w_1 = entropia(hchist(1,p_1(1),p_1(2),:));
                w_2 = entropia(hchist(2,p_1(1),p_1(2),:));
                w_3 = entropia(hchist(3,p_1(1),p_1(2),:));
            elseif p_1(1) ~= p_4(1) & p_1(2) ~= p_4(2)
                w_1 = entropia(hchist(1,p_1(1),p_1(2),:) + hchist(1,p_1(1)+1,p_1(2),:) + hchist(1,p_4(1),p_4(2),:) + hchist(1,p_4(1)-1,p_4(2),:));
                w_2 = entropia(hchist(2,p_1(1),p_1(2),:) + hchist(2,p_1(1)+1,p_1(2),:) + hchist(2,p_4(1),p_4(2),:) + hchist(2,p_4(1)-1,p_4(2),:));
                w_3 = entropia(hchist(3,p_1(1),p_1(2),:) + hchist(3,p_1(1)+1,p_1(2),:) + hchist(3,p_4(1),p_4(2),:) + hchist(3,p_4(1)-1,p_4(2),:));
            else
                w_1 = entropia(hchist(1,p_1(1),p_1(2),:) + hchist(1,p_4(1),p_4(2),:) );
                w_2 = entropia(hchist(2,p_1(1),p_1(2),:) + hchist(2,p_4(1),p_4(2),:) );
                w_3 = entropia(hchist(3,p_1(1),p_1(2),:) + hchist(3,p_4(1),p_4(2),:) );
            end
            
            m = [];
            for i=1:3
                for j=1:3
                    x = int32(red(i,j));
                    y = int32(green(i,j));
                    z = int32(blue(i,j));
                    
                    t = x*w_1 + y*w_2 + z*w_3;
                    v = [ t red(i,j) green(i,j) blue(i,j) ];
                    m = [m ; v];
                end 
            end
            %ACA YA TENEMOS TODAS LAS TUPLAS Ti, Ri, Gi, Bi EN LA MATRIZ m
            E = median(m); %E DE TUPLA ELEGIDA
            img(fila, columna, 1)  = E(2);
            img(fila, columna, 2)  = E(3);
            img(fila, columna, 3)  = E(4);
        end
    end
    img = img(2:M-1,2:N-1,:); 
    retorno= img;
    
end