function [] = agrega_ruido()
%m = 0;
ruido = 0.005;    

while ruido <= 0.165 
    dinfo = dir(char("imagenes/original/*"));
    for K = 3 : length(dinfo)
        imname = dinfo(K).name;
        im = imread(strcat(char("imagenes/original/"),imname));
        %introduce random salt and pepper noise to the image
        im_con_ruido = imnoise(im,'speckle', ruido);
        imwrite(im_con_ruido,strcat(char("imagenes/speckle/"+num2str(ruido)+"/"),imname))
    end
    ruido = ruido + 0.01;
end
