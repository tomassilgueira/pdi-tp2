%Calcula la entropia recibiendo como parametro un histograma, a diferencia
%de la funcion integrada entropy() que recibe una imagen como parametro.
function E = entropia(h)   
h = h/sum(h);
E = -sum(h.*log2(h));