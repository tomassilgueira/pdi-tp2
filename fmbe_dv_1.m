function retorno = fmbe_dv_1(img)
    img = padarray(img, [1, 1], 'replicate');     % ACA SE PADDEA LA IMAGEN RECIBIDA, Y SE RELLENAN CON EL METODO DE REPLICA LOS ESPACIOS AGREGADOS
    
    [M,N,Q] = size(img);    %  Q No sirve para nada, hace que las cosas funcionen nada mas
    K = 3;
    L = 3;
    red = img(:,:,1);                 % EXTRAE LOS
    green = img(:,:,2);               % SEGMENTOS R, G y B
    blue = img(:,:,3);  
    w_1 = double(entropy(red));                     % EXTRAE LAS ENTROPIAS
    w_2 = double(entropy(green));                   % w1, w2 y w3 DE CADA
    w_3 = double(entropy(blue));                    % SEGMENTO
    
    for fila=2:(M-1)
        for columna=2:(N-1)
            
            f = fila - floor(K/2);                   % CALCULA LOS
            fu = fila + floor(K/2);                  % LIMITES DE LOS
            c = columna - floor(L/2);                % SEGMENTOS
            cu = columna + floor(L/2);               
            
            red = img(f:fu,c:cu,1);                 % EXTRAE LOS
            green = img(f:fu,c:cu,2);               % SEGMENTOS R, G y B
            blue = img(f:fu,c:cu,3);                % QUE AGARRA LA MASCARA
            
            m = [];
            for i=1:3
                for j=1:3
                    x = int32(red(i,j));
                    y = int32(green(i,j));
                    z = int32(blue(i,j));
                    t = x*w_1 + y*w_2 + z*w_3;
                    v = [ t red(i,j) green(i,j) blue(i,j) ];
                    m = [m ; v];
                end 
            end
            %ACA YA TENEMOS TODAS LAS TUPLAS Ti, Ri, Gi, Bi EN LA MATRIZ m
            E = median(m); %E DE TUPLA ELEGIDA
            img(fila, columna, 1)  = E(2);
            img(fila, columna, 2)  = E(3);
            img(fila, columna, 3)  = E(4);
        end
    end
    img = img(2:M-1,2:N-1,:); 
    retorno= img;
end
%HACER MATRICES PARA ENTROPIAS PARA CADA COMPONENTE DE COLOR