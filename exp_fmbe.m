function [] = exp_fmbe()
ruido = 0.005;    

while ruido <= 0.165 
    dinfo = dir(strcat(char("imagenes/gaussian/"+num2str(ruido)+"/")));
    for K = 3 : length(dinfo)
        imname = dinfo(K).name;
        im = imread(strcat(char("imagenes/gaussian/"+num2str(ruido)+"/"),imname));
        %aplica el filtro sobre la imagen
        im_filtrada = fmbe(im);
        imwrite(im_filtrada,strcat(char("imagenes/fmbe/gaussian/"+num2str(ruido)+"/"),imname))
    end
    ruido = ruido + 0.01;
end