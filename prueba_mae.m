function [] = prueba_mae()

ruido = 0.005;    
sumatoria = 0;
while ruido <= 0.055 
    dinfo1 = dir(char("imagenes/original/*"));
    dinfo2 = dir(strcat(char("imagenes/fmbe/gaussian/"+num2str(ruido)+"/*")));%prueba
    %dinfo2 = dir(char("imagenes/fmbe/gaussian/0.005/*"));
    for K = 3 : length(dinfo1)
        imname1 = dinfo1(K).name;
        imname2 = dinfo2(K).name;
        im1 = imread(strcat(char("imagenes/original/"),imname1));
        im2 = imread(strcat(strcat(char("imagenes/fmbe/gaussian/"+num2str(ruido)+"/")),imname2));%prueba
        %im2 = imread(strcat(char("imagenes/fmbe/gaussian/0.005/"),imname2));
        %introduce random salt and pepper noise to the image
        builtInMAE = sum(mae(im1-im2));
        sumatoria = sumatoria + builtInMAE;
        
    end
    ruido = ruido + 0.01;
    tamanho = length(dinfo1)-2;
    maepromedio = sumatoria / tamanho
end